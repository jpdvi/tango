const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const app = express();
const Router = require('./router');
const mongoose = require('mongoose');
const config = require('./config.js');
const cors = require('cors');
'use strict';

module.exports = class Server{
  constructor()
  {
    this.app = express();
    this.router = null
    this.port = 3001;
  }

  async start(){
    //this.app.use(bodyParser.json({type: '*/*', limit: '50mb'}));
    this.app.use(cors());
    this.app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    //  res.header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS')
      next();
    });

    this.router =new Router(this.app, express);
    this.router.initRouter();

    await mongoose.connect(config.database)
    .then((info)=>{
      console.log(`Mongoose connected to : ${config.database}`)
      //app.use(morgan('combined'));
    })
    .catch((err)=>{
      console.error('mongoose error : '+ err.stack)
    })

    const server = http.createServer(this.app);
    server.listen(this.port);
    console.log(`Server Listening on ${this.port}`);
  }
}
