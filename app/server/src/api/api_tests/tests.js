const Router  = require('../router');
const express = require('express');
const app     = express();
const assert  = require('assert');
const Compiler= require('../classes/compiler.js')
const Deployer= require('../classes/deployer.js')
let router;
var endpoints = ['/', '/accounts']

beforeEach(function(){
  router = new Router(app, express);
  router.initRouter()
});

describe('Router: ', function(){
  it('is running', function(){
    assert.ok(router.running())
  })

  it('endpoints working', function(){
    endpoints.forEach((endpoint)=>{
      console.log(endpoint)
    })
  })
})

describe('Compiler: ', function(){
  this.timeout(15000)
  it('can compile',  function(){
    const run_compiler = async function(){
      const comp = new Compiler();
      var i = await comp.compile()
      assert.ok(i);
    }
    const i = run_compiler();
  })
})


describe('Deployer', function(){
  it('can deploy', function(){
    const run_deployer = async function(){
      const dep = new Deployer();
      const accounts = await dep.getAccounts();
      const contract = await dep.deploy({'sender':accounts[0], 'rec': 'waf', 'info':'test'});
    }
    const i = run_deployer();
  })
})
