const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Inbox  = require('./inbox');
const Schema = mongoose.Schema;
const generateEthUser = require('../methods/generate_eth_user');
const AppUserSchema = new Schema({
  active: {
    type: Boolean,
    required: false
  },
  name: {
    type: String,
    required: false
  },
  first_name:{
    type: String,
    required:true
  },
  last_name:{
    type:String,
    required:true
  },
  email: {
    type: String,
    required: false
  },
  password:{
    type: String,
    required: true
  },
  known_addresses:{
    type: [String],
    required: false
  },
  address:{
    type:String,
    required:false
  },
  private_key:{
    type:String,
    required:false
  },
  mnemonic:{
    type: [String],
    required: false
  },
  notifications:[]
,
  messages:{
    type:[]
  }
});

AppUserSchema.pre('save', function(next){
    const user = this;
    let geneth = async()=>{
      let eu = await generateEthUser();
      return eu
    }

    geneth()
    .then((ethuser)=>{
      user.mnemonic    = ethuser.mnemonic;
      user.address     = ethuser.eth.address;
      user.private_key = ethuser.eth.privateKey;

      bcrypt.genSalt(10,(err, salt)=>{
        if(err){ return next(err); }
        bcrypt.hash(user.password, salt, null, function(err, hash){
          if(err) {return next(err)}
          user.password = hash;
          next();
        });
      });
    })





  // bcrypt.genSalt(10, (err, salt)=>{
  //   if(err){ return next(err); }
  //
  //   bcrypt.hash(user.mnemonic, salt, null, function(err, hash){
  //     if(err) { return next(err)}
  //     user.mnemonic = hash;
  //     next();
  //   });
  // });
});

AppUserSchema.methods.comparePassword = function(candidatePassword, callback){

  bcrypt.compare(candidatePassword, this.password, (err, isMatch)=>{

    if(err) {return callback(err); }

    callback(null, isMatch);
  })
}

AppUserSchema.methods.compareMnemonic = function(candidateMnemonic, callback){
  bcrypt.compare(candidateMnemonic, this.mnemonic, (err, isMatch)=>{
    if(err) {return callback(err); }
    callback(null, isMatch);
  })
}

const AppUser = mongoose.model('AppUsers', AppUserSchema);

module.exports = AppUser;
