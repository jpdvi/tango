const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InboxMessageSchema = new Schema({
  inbox_id:{
    type:String,
    required:true
  },
  user_id:{
    type:String,
    required:true
  },
  contra_user_id:{
    type:String,
    required:true
  },
  message:{
    type:String,
    required:false
  }
}, {_id:false});


const InboxMessage = mongoose.model('InboxMessages', InboxMessageSchema);
module.exports = InboxMessage;
