const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContractSchema = new Schema({
  users: {
    type: [String],
    required: true
  },
  cid  : {
    type:String,
    required:false
  },
  body : {
    type:[],
    required:true
  },
  confirms: [],
  owner: {
    type: String,
    required:true
  },
  contra: {
    type: [String],
    required:true
  }
});
const Contract = mongoose.model('Contracts', ContractSchema);

module.exports = Contract
