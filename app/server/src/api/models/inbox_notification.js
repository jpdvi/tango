const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InboxNotificationSchema = new Schema({
  recipient_id:{
    type:String,
    required:true
  },
  sender_id:{
    type:String,
    required:true
  },
  message:{
    type:String,
    required:true
  },
  note_type:{
    type:String,
    required:true
  },
  priority:{
    type:Number,
    required:false,
    default:0
  }
},{_id:false})

const InboxNotification = mongoose.model('InboxNotifications', InboxNotificationSchema);
module.exports = InboxNotification;
