const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const InboxMessage = require('./inbox_message');
const InboxNotification = require('./inbox_notification');

const InboxSchema = new Schema({
  messages:      [{type:mongoose.Schema.Types.Array, ref:'InboxMessage',    default:[]}],
  notifications: [{type:mongoose.Schema.Types.Array, ref:'InboxNotification',default:[]}]
}, {_id:false});

const Inbox = mongoose.model('Inboxes', InboxSchema);
module.exports = Inbox;
