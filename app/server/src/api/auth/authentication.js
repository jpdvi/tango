const AppUser = require('../models/app_user');

const jwt = require('jwt-simple');
const config = require('../config')

function userToken(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

exports.login =(req, res, next)=>{
  console.log(req.body)
  let current_user = null;
  AppUser.findOne({email:req.body.email}, (err, user)=>{
    let current_user={'_id':user._id, 'first_name':user.first_name, 'last_name': user.last_name, 'known_accounts':user.known_accounts}
    res.send({ token: userToken(req.user), user:current_user});
  })
}

exports.signup = (req, res, next)=>{
 //Does user exist?
 console.log(req.body)
 const email    = req.body.email;
 const password = req.body.password;
 const mnemonic = req.body.mnemonic;
 const first_name=req.body.first_name;
 const last_name =req.body.last_name;
 //if user exists return error
AppUser.findOne({email:email}, (err, user)=>{
  if(err) throw err;

  if(user){
    console.log(user)
    return res.status(422).send({error: 'Account is in use'})
  }

  if(!email || !password){
    return res.status(422).send({error:'Must Provide Email and Password'})
  }

  const newuser = new AppUser({
    email: email,
    password: password,
    mnemonic: mnemonic,
    first_name:first_name,
    last_name:last_name
  })

  newuser.save((err)=>{
    if (err) return next(err)
    res.json({token: userToken(newuser)});
  })
})

}
