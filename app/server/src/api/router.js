const express = require('express');
const config = require('./config');
const Promise = require('promise');
const moment = require('moment');
const ganache = require('ganache-cli');
const Web3 = require('web3')
const web3  = new Web3(ganache.provider());
const passport = require('passport');
const mongoose = require('mongoose');

const passportService = require('./auth/passport');
const Authentication = require('./auth/authentication');

const Compiler = require('./classes/compiler.js');
const deploy   = require('./methods/deploy.js');

const bodyParser = require('body-parser');
const requireAuth = passport.authenticate('jwt', {session: false});
const requireLogin = passport.authenticate('local', {session: false});

const AppUser = require('./models/app_user');
const Contract = require('./models/contract');

module.exports = class Router
{
  constructor(app, express){
    this.app     = app;
    this.express = express;
    this.router  = this.express.Router();
    this.is_running = false;
    this.compiler   = new Compiler();
  }

  running(){
    return this.is_running
  }

  initRouter(){
    this.is_running = true;
    this.app.use(bodyParser.urlencoded({extended: true}));
    this.app.use(bodyParser.json());
    this.router.get('/', function(req,res,next){
      res.send('Confirmed');
    });

    this.router.post('/login', requireLogin, Authentication.login);
    this.router.post('/create', Authentication.signup);

    this.router.get('/accounts', async (req,res,next)=>{
      this.accounts =  await web3.eth.getAccounts();
      res.send(this.accounts);
    });

    this.router.post('/execute', async (req, res, next)=>{
      //console.log(req.body.test);
      let contract = await this.compiler.compile();
      res.send(contract)
    })

    this.router.post('/users', async (req,res, next)=>{
      let u;
      //console.log(req.body)
      if (req.body.user)
      {
        if (req.body.user == -999)
        {
          console.log('found null')
          res.send([])
          return
        }
        let u = req.body.user
      //  console.log(u)
      }
      else{
        let u = {}
      }
      const query = new Promise((resolve, reject)=>{

        AppUser.find({'first_name': new RegExp(u,'i')} , (err, users)=>{
          resolve(users)
        })
      })
      .then((results)=>{
        res.send(results)
      })
      .catch((error)=>{
        console.log(error)
      })
    })
    this.router.post('/inbox', async (req, res, next)=>{

        AppUser.findOne({'_id':req.body.auth.current_user}, (err, user)=>{

          let notes = user.notifications.filter((note)=>{
            return(note.confirmed === false)
          })
          res.send({'notifications':notes, 'messages':user.messages})
        })
    });

    this.router.post('/add_confirm', async(req, response , next)=>{

        Contract.findByIdAndUpdate(req.body.cid,{$push:{confirms:req.body.auth.current_user}}, {safe:true,upsert:true}, (err, contract)=>{
        //  AppUser.update({'_id':req.body.current_user}, {$set:{notifications:})

        AppUser.findOne({'_id':contract.owner}, (err, owner)=>{
          console.log(owner.private_key);
          const params = {'sender': owner.private_key,
                          'rec'   : req.body.auth.current_user,
                          'info'  : JSON.stringify(contract.body),
                          'skey'  : owner.private_key}

          try{
            const dep = deploy(params)
            .then((res)=>{
              //Favorite.update( {cn: req.params.name}, { $pullAll: {uid: [req.params.deleteUid] } } )
              //{ "items" : { id: 23 } } }
               AppUser.update({'_id':req.body.auth.current_user}, {$pull:{notifications:{cid:req.body.cid}}}, (err, user)=>{

               })
            })
          }
          catch(err)
          {
            console.log(err);
          }
        })
        })

        response.send({'success':true})
    })

    this.router.post('/commit_contract', async(req,res,next)=>{

      let contract_data = req.body.data.map((row)=>{
       return ({'type':row[0], 'data':row[1]})
      })

      let contras = await req.body.users.filter((user)=>{
        return(user._id != req.body.current_user.current_user)
      })

      let contract = new Contract({
        users:req.body.users,
        cid:'1234',
        body:contract_data,
        confirms:[],
        owner:req.body.current_user.current_user,
        contra:contras
      });

      await contract.save((err, msg)=>{
        contras.forEach((contra)=>{
          let notification = {'time':moment(), 'cid':msg._id.toString(), 'contra':req.body.current_user.current_user,
          'contra_first_name':req.body.current_user.first_name, 'contra_last_name':req.body.current_user.last_name, 'confirmed':false, 'viewed':false}
          AppUser.findByIdAndUpdate(contra._id,{$push:{notifications:notification}}, {safe: true, upsert: true}, (err, user)=>{
          })
        })
      })
      res.send('success')
    });

    this.router.post('/user_status', async (req, res , next)=>{
      const id = req.body._id
      let   user_obj = {
        'known_addresses':null,
        'first_name'     :null,
        'last_name'      :null
      }
      await AppUser.findOne({_id:id}, async(err, user)=>{
        user_obj.known_addresses = user.known_addresses;
        user_obj.first_name      = user.first_name;
        user_obj.last_name       = user.last_name;
      })
      res.send(user_obj);
    })

    this.router.post('/deploy', async (req, res, next)=>{

      if(req.body.sender == null || req.body.rec == null || req.body.info == null)
      {
        res.send({'response_type':'ERROR', 'code':'invalid params'})
      }
      const params = {'sender': req.body.sender,
                      'rec'   : req.body.rec,
                      'info'  : req.body.info}

      const dep = await deploy(params);
      res.send(dep)
    })

    this.router.get('/logo', async (req,res,next)=>{
      res.sendFile('/Users/jpdvi/Projects/tango/app/server/src/api/public/cryptango_inverse.jpg')
    })

    this.router.get('/landing_logo', async(req,res,next)=>{
      res.sendFile('/Users/jpdvi/Projects/tango/app/server/src/api/public/full_logo_inverse.jpg')
    })

    this.app.use('/',this.router);
  }
}
