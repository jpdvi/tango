const HDWalletProvider = require("truffle-hdwallet-provider");
const Web3             = require("web3");
const config           = require('../config.js');
const Compiler         = require('./compiler.js');
const randomwords      = require('random-words');
'use strict'

module.exports = class Deployer{
  constructor(skey){

    this.network  = config.rinkeby;
    this.secret   = skey;
//    this.web3     = new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/anBPwop4ClhwUkAwyZEx"));
    this.account  = '0x3c1696cf0EceC4de73dbF1907D3423A6d4139c53'
    this.mnemonic = 'earth upgrade wealth lyrics banana soup improve fortune vapor artefact myth deny'
    this.provider = new HDWalletProvider(this.mnemonic,'https://rinkeby.infura.io/anBPwop4ClhwUkAwyZEx' )
    this.web3     = new Web3(this.provider);
    this.accounts = null
    this.result   = null;
    this.params   = null;
  }

  async getAccounts(skey){
    let i = await this.web3.eth.accounts.privateKeyToAccount(skey);
    return i
  }
  // "consider use idea cabin",
  // 		"though",
  // 		"travel",
  // 		"image",
  // 		"course",
  // 		"maybe",
  // 		"sign",
  // 		"these",
  // 		"fear"
  async deploy(parameters){
    try {
      //this.provider = new HDWalletProvider(this.account,'https://rinkeby.infura.io/anBPwop4ClhwUkAwyZEx')
      //this.web3     = new Web3(this.provider);
      //this.web3     = new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/anBPwop4ClhwUkAwyZEx"));
      this.params     = parameters;
      const params    = parameters;
      const compiler   = new Compiler();
      const contract   = await compiler.compile();

      this.result = await new this.web3.eth.Contract(JSON.parse(contract.interface))
      .deploy({data:contract.bytecode, arguments: [this.account, params.rec, params.info]}, )
      .send({gas: '1000000', from: this.account})
      .catch((e)=>{
        console.log("Promise Error DEPLOY: " + e);
        throw e;
      })
    } catch (e) {
      console.log('Deployment Error: ' + e);
    } finally {
      console.log('Deployed to : ', this.result.options.address);
      return this.result.options.address;
    }
  }
}
