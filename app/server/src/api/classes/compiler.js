const path = require('path');
const fs   = require('fs');
const solc = require('solc');

module.exports = class Compiler{
  constructor(){
    this.contractPath = path.resolve(process.env['HOME'],'Projects', 'tango','ethereum','contracts','tango.sol');
    this.contractName = 'TangoContract'
    this.source       = fs.readFileSync(this.contractPath, 'utf8')
  }

  compile(){
    //console.log(`Compiling : ${this.contractName}`);
    return solc.compile(this.source, 1).contracts[`:${this.contractName}`]
  }
}
