const Web3  = require("web3");
const randomwords = require('random-words')
const md5         = require('md5')

function generateEthUser(){
let web3 = new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/B7VPB7CG1tegp1V8bvPr"));
  let mn = randomwords(12);
  let r = mn.join();
  const  i = web3.eth.accounts.create(r);
  return({'eth':i, 'mnemonic':mn})
}

module.exports = generateEthUser;
