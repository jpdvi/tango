const Compiler  = require('../classes/compiler.js')
const Deployer  = require('../classes/deployer.js')

module.exports = async function(parameters)
{

  const run_deployer = async function(params){

   const dep = new Deployer(params.skey);
   // const accounts = await dep.getAccounts(params.skey);
   // params.sender=accounts.address
   const contract = await dep.deploy(params);
   return contract
  }
  const i = await run_deployer(parameters);
  return i
}
