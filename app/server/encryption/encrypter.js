const crypto = require('crypto');

class Encrypter
{
  constructor(secret)
  {
    this.secret = secret;
    this.algorithm = 'aes256'
  }

  encrypt(message)
  {
    const cipher = crypto.createCipher(this.algorithm,this.secret);
    let crypted  = cipher.update(message, 'utf8', 'hex')
    crypted += cipher.final('hex')
    return crypted;
  }

  decrypt(encrypted)
  {
    const decipher = crypto.createDecipher('aes256', this.secret);
    let decrypted = decipher.update(encrypted, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
  }
}


var e = new Encrypter('052118');
var i = e.encrypt("asdjlfkasjflkdsajfsadkjfsdk");
var d = e.decrypt(i);
console.log(d);
