pragma solidity ^0.4.0;
contract TangoContract{
    string public owner;
    string public info;
    string public contra_party;

    struct Payment{
        string sender;
        uint   value;
    }

    struct Contract{
        string sender;
        string data;
        Payment payment;
    }

    function TangoContract (string sender, string receiver, string initial_info) public{
      owner        =  sender;
      contra_party = receiver;
      info         = initial_info;
    }

    function getOwner () public view returns(string){
        return owner;
    }
}
