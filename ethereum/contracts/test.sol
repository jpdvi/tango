pragma solidity ^0.4.17;
contract Inbox {
    string public message;

    function Inbox(string initialMessage) public
    {
        message = initialMessage;
    }

    function setMessage(string newMessage) public
    {
        message = newMessage;
    }

    function encryptMessage(string imessage) private returns(string)
    {
        return imessage;
    }

    function getMessage() public view returns(string)
    {
        var i = encryptMessage("test_msg");
        return i;
    }
}
